const axios = require('axios').default;

exports.getReq = async(url,options={})=>{
    try {
        const result = await axios.get(url,{params:options});
        return result
    } catch (error) {
        console.log(error);
        return error;
    }
}
exports.dltReq = async(url,options={})=>{
    try {
        const result = await axios.delete(url,{config:options});
        return result
    } catch (error) {
        console.log(error);
        return error;
    }
}