
const express = require('express'),
    app = express(),
    cors = require('cors'),
    cookieParser = require('cookie-parser'),
    moduleName = "'wasim'",
    indexRouter = require(`${__routes}/`);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:false}));
//app.use(cookieParser());

// app.use((req,res,next)=>{
//     if(!req.method.includes("POST,GET,DELETE,PUT,PATCH")) res.status(403).json({"message":"Mehtod Not allowed"});
//         next();
// })
app.use('/api/v1',indexRouter)
module.exports=app;