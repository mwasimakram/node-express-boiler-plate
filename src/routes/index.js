const router = require('express').Router(),
      gistsCtrl = require(`${__controller}/gists`);

require(`${__routes}/gists`)(router,gistsCtrl);

module.exports = router;