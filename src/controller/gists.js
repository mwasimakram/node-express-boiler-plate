const config = require(`${__config}/config`);
const {getReq,dlt} = require('../utils/httpReq');
exports.getGitsList = async(req,res)=>{
    try {
        let resp = await getReq(`${config.url.GET_GISTS_LIST}`);
        res.json({success:true,data:resp});
    } catch (error) {
        res.json({success:false,message:error})
    }
}
exports.getGistsById = async(req,res)=>{
    try {
        let params={
            gist_id:req.params.id
        }
        let resp = await getReq(`${config.url.GET_GIST_BY_ID}`,params);
        res.json({success:true,data:resp});
    } catch (error) {
        res.json({success:false,message:error})
    }
}
exports.deleteGists = async(req,res)=>{
    try {
        let params={
            gist_id:req.params.id
        }
        let resp = await dltReq(`${config.url.DELETE_GIST_BY_ID}`,params);
        res.json({success:true,data:resp});
    } catch (error) {
        res.json({success:false,message:error})
    }
}